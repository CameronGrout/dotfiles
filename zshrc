# Path to your oh-my-zsh installation.
export ZSH=$HOME/.oh-my-zsh

# Set name of the theme to load.
ZSH_THEME="geoffgarside"

setopt NO_cdable_vars

# Uncomment the following line to use case-sensitive completion.
# CASE_SENSITIVE="true"

# Uncomment the following line to use hyphen-insensitive completion. Case
# sensitive completion must be off. _ and - will be interchangeable.
# HYPHEN_INSENSITIVE="true"

# Uncomment the following line to disable bi-weekly auto-update checks.
# DISABLE_AUTO_UPDATE="true"

# Uncomment the following line to change how often to auto-update (in days).
# export UPDATE_ZSH_DAYS=13

HIST_STAMPS="yyyy-mm-dd"

# Which plugins would you like to load? (plugins can be found in ~/.oh-my-zsh/plugins/*)
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=(colored-man-pages git git-extras)

export PATH="/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games"

source $ZSH/oh-my-zsh.sh

# Preferred editor for local and remote sessions
if [[ -n $SSH_CONNECTION ]]; then
    export EDITOR='vim'
fi

TEXLIVE_DIR=""

if [ -d "/usr/local/texlive/2015/" ]; then
    TEXLIVE_DIR="/usr/local/texlive/2015"
elif [ -d "/usr/local/texlive/2016/" ]; then
    TEXLIVE_DIR="/usr/local/texlive/2016"
fi

if [ ! -z "$TEXLIVE_DIR" ]; then
    TEX_DIR=""

    for bin_dir in $TEXLIVE_DIR/bin/*; do
        TEX_DIR+="$bin_dir:"
    done

    if [ ! -z "$TEX_DIR" ]; then
        PATH="$TEX_DIR$PATH"
    fi

    unset MANPATH
    export MANPATH="$TEXLIVE_DIR/texmf-dist/doc/man:$(manpath)"

    export INFOPATH="$TEXLIVE_DIR/texmf-dist/doc/info:$INFOPATH"
fi

# Prepend the user executables to the PATH
if [ -d "$HOME/bin" ]; then
    PATH="$HOME/bin:$PATH"

    if [ -f "$HOME/bin/svim" ]; then
        alias vim='svim'
    fi

    if [ -f "$HOME/bin/sgvim" ]; then
        alias gvim='sgvim'
    fi
fi

# Aliases
alias ls='ls -lh --color=auto'
alias ll='ls -lah'

# Print message(s) of the day if they exist
if [[ -d ~/.motd ]]; then
    for motd in ~/.motd/*; do
        cat $motd
    done
fi

