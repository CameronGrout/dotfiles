""" ViM & GViM configuration file (vimrc)
"""
""" @author Cameron Grout <cameron@sporadic.co.nz>


"====[Need to set nocompatible before pathogen stuff]====
set nocp



"====[ Load Pathogen ]====
runtime bundle/vim-pathogen/autoload/pathogen.vim
execute pathogen#infect()



"====[ Setup ]====
set noerrorbells
set ttyfast
set lazyredraw
set t_Co=256
set encoding=utf-8
set nowrap
set list
set listchars=tab:»·,eol:¬,nbsp:·,precedes:…,extends:…,trail:·
set matchpairs+=<:>
set mouse=a
set mousehide
set cursorline
set number
set relativenumber
set title
set backspace=2
set scrolloff=5
set splitright
set splitbelow
set laststatus=2
set magic
set incsearch
set hlsearch
set swapfile
set ignorecase
set smartcase

let mapleader=","

filetype plugin indent on
set tabstop=4
set shiftwidth=4
set softtabstop=4
set expandtab

syntax on



"====[ Colorscheme stuff ]====
colorscheme wombat256
if has("gui_running")
    colorscheme wombat
    set guioptions=
endif

" Only highlight lines that exceed 80c. Thanks Damian Conway
highlight ColorColumn ctermbg=magenta guibg=magenta
call matchadd('ColorColumn', '\%81v', 100)



"====[ General keybinds ]====
nnoremap <silent><Leader>n :set relativenumber!<cr>



"====[ Toggle comments on lines in Visual mode ]====
" Work out what the comment character is, by filetype...
autocmd FileType           *sh,awk,python,perl,perl6,ruby    let b:cmt = exists('b:cmt') ? b:cmt : '#'
autocmd FileType           cpp,c,h,hpp,cs                    let b:cmt = exists('b:cmt') ? b:cmt : '//'
autocmd FileType           vim                               let b:cmt = exists('b:cmt') ? b:cmt : '"'
autocmd FileType           tex,latex,plaintex                let b:cmt = exists('b:cmt') ? b:cmt : '%'
autocmd BufNewFile,BufRead *.vim,.vimrc                      let b:cmt = exists('b:cmt') ? b:cmt : '"'
autocmd BufNewFile,BufRead *                                 let b:cmt = exists('b:cmt') ? b:cmt : '#'
autocmd BufNewFile,BufRead *.p[lm],.t                        let b:cmt = exists('b:cmt') ? b:cmt : '#'

function! ToggleBlock () range
    let comment_char = exists('b:cmt') ? b:cmt : '#'
    let linenum = a:firstline

    " Get all the lines, and decide their comment state by examining the first...
    let currline = getline(a:firstline, a:lastline)
    if currline[0] =~ '^' . comment_char
        " If the first line is commented, decomment all...
        for line in currline
            let repline = substitute(line, '^' . comment_char, "", "")
            call setline(linenum, repline)
            let linenum += 1
        endfor
    else
        " Otherwise, encomment all...
        for line in currline
            let repline = substitute(line, '^\('. comment_char . '\)\?', comment_char, "")
            call setline(linenum, repline)
            let linenum += 1
        endfor
    endif
endfunction

" Set up the relevant mappings
nmap <silent> <Leader># :call ToggleComment()<CR>j0
vmap <silent> <Leader># :call ToggleBlock()<CR>



"====[ Flash searched item under cursor ]====
function! HLNext (blinktime)
    highlight WhiteOnRed ctermfg=white ctermbg=red guifg=white guibg=red
    let [bufnum, lnum, col, off] = getpos('.')
    let matchlen = strlen(matchstr(strpart(getline('.'),col-1),@/))
    let target_pat = '\c\%#\%('.@/.'\)'
    let blinks = 3
    for n in range(1,blinks)
        let red = matchadd('WhiteOnRed', target_pat, 101)
        redraw
        exec 'sleep ' . float2nr(a:blinktime / (2*blinks) * 500) . 'm'
        call matchdelete(red)
        redraw
        exec 'sleep ' . float2nr(a:blinktime / (2*blinks) * 500) . 'm'
    endfor
endfunction

nnoremap <silent> n n:call HLNext(0.4)<cr>
nnoremap <silent> N N:call HLNext(0.4)<cr>



"====[ Set up bindings for copy/cut/past to system clipboard ]====
map <Leader>p "+p <cr>
map <Leader>y "+y <cr>
map <Leader>d "+d <cr>



"====[ Set up function and bindings for sessions that save colorscheme ]====
function! Mkusefulsession ()
    let l:sessionname = "session.vimsess"
    exe "mksession! ".l:sessionname
    silent exe '!echo colorscheme '.g:colors_name.' >> '.l:sessionname
    redraw!
endfunction

nmap <silent> <Leader>s :call Mkusefulsession()<CR>



"====[ Platform specific options ]====
if has("win32")
    set gfn=Source_Code_Pro:h9:cANSI
    silent execute '!mkdir "'.$HOME.'\_vim_backups"'
    set backupdir=$HOME\_vim_backups\\
    set directory=$HOME\_vim_backups\\
    let g:vimtex_view_general_viewer='SumatraPDF'
    let g:vimtex_view_general_options = '-forward-search @tex @line @pdf'
    let g:vimtex_view_general_options_latexmk = '-reuse-instance'
elseif has("unix")
    set gfn=Source\ Code\ Pro\ 11
    silent execute '!mkdir -p $HOME/.vim_backups'
    set backupdir=$HOME/.vim_backups//
    set directory=$HOME/.vim_backups//
    let g:vimtex_view_general_viewer='xdg-open'
endif



"====[ File specific executions ]====
" Automatically make files executable if they contain a shebang on the first line
if has("unix")
    au BufWritePost * if getline(1) =~ "^#!" | if getline(1) =~ "/bin/" | silent execute "!chmod a+x <afile>" | endif | endif
endif

" Automatically source vimrc if it is modified by this vim instance
autocmd! BufWritePost $MYVIMRC source $MYVIMRC



"====[ UltiSnips configuration ]====
let g:UltiSnipsExpandTrigger="<tab>"
let g:UltiSnipsJumpForwardTrigger="<tab>"
let g:UltiSnipsJumpBackwardTrigger="<s-tab>"



"====[ Vimtex configuration ]====
let g:vimtex_fold_enabled=0
let g:vimtex_quickfix_open_on_warning=0
let g:vimtex_mappings_enabled=0
let g:tex_flavor = 'latex'



"====[ Vimwiki configuration ]====
let g:vimwiki_list = [{
  \ 'path': '$GDRIVE\\Documents\\vimwiki\\', 
  \ 'index': 'main',
  \ 'template_path': '$GDRIVE\\Documents\\vimwiki\\templates\\',
  \ 'template_default': 'default',
  \ 'template_ext': '.tpl',
  \ 'auto_toc': 1, 
  \ 'auto_export': 1}]

autocmd FileType vimwiki set wrap
autocmd FileType vimwiki set linebreak
autocmd FileType vimwiki set nolist
autocmd FileType vimwiki set colorcolumn=

autocmd BufReadPost *.tex set syntax=tex
autocmd FileType tex,bib,plaintex nnoremap <silent><Leader>lm :VimtexCompile<cr>
autocmd FileType tex,bib,plaintex nnoremap <silent><Leader>lc :VimtexClean<cr>
autocmd FileType tex,bib,plaintex nnoremap <silent><Leader>lw :VimtexWordCount<cr>
autocmd FileType tex,bib,plaintex nnoremap <silent><Leader>lv :VimtexView<cr>
