#!/bin/bash

DOTFILES=$HOME/dotfiles
OH_MY_ZSH=$HOME/.oh-my-zsh
USER_FONTS=$HOME/.fonts
INSTALL_BACKUP=$HOME/dotfiles_$(date +%F_%T)
OS="Linux"

###
### OS & DISTRO DETECTION
###

if [[ ! "$OSTYPE" == "linux-gnu" ]]; then
    echo "Not running on GNU/Linux, exiting" >&2
    exit
fi


if [[ `uname -v` == *"Ubuntu"* ]]; then
    OS="Ubuntu"
    USER_FONTS=$HOME/.fonts
elif [[ `uname -r` == *"ARCH"* ]]; then
    OS="Arch"
    USER_FONTS=$HOME/.local/share/fonts
fi



###
### INSTALL REQUIRED SOFTWARE
###

# Install requisite programs using the appropriate package manager
if [[ "$OS" == "Ubuntu" ]]; then
    sudo apt-get install git vim-gnome zsh curl
elif [[ "$OS" == "Arch" ]]; then
    sudo pacman -S git vim zsh curl gvim
else
    echo "No package manager configuration for detected distribution found. Exiting" >&2
    exit
fi

# Make sure we are in the correct directory
cd $HOME



###
### INSTALL OH-MY-ZSH
###

if [ ! -d "$OH_MY_ZSH" ]; then
    # Fetch oh-my-zsh and install it
    sh -c "$(curl -fsSL https://raw.github.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
    # Installer will automatically set shell to ZSH if possible
fi



###
### INSTALL FONTS
###

# Ensure that the local user fonts directory exists
mkdir -p $USER_FONTS

FONTS=$(find $DOTFILES/fonts -type f -name '*.ttf')

for i in $FONTS; do
    ln -sf $i $USER_FONTS/`basename $i`
done

# Update the font-cache
fc-cache -f -v



###
### UPDATE DOTFILES
###

mkdir $INSTALL_BACKUP

# Move dirs and files that will foul stuff up
if [ -d "$HOME/.vim" ]; then
    mv $HOME/.vim $INSTALL_BACKUP
fi

if [ -f "$HOME/.vimrc" ]; then
    mv $HOME/.vimrc $INSTALL_BACKUP
fi

if [ -f "$HOME/.zshrc" ]; then
    mv $HOME/.zshrc $INSTALL_BACKUP
fi

if [ -f "$HOME/.lftprc" ]; then
    mv $HOME/.lftprc $INSTALL_BACKUP
fi

if [ -f "$HOME/.zshrc.pre-oh-my-zsh" ]; then
    mv $HOME/.zshrc.pre-oh-my-zsh $INSTALL_BACKUP
fi

# Link in the files
ln -s $DOTFILES/vim/ $HOME/.vim
ln -s $DOTFILES/vimrc $HOME/.vimrc
ln -s $DOTFILES/zshrc $HOME/.zshrc
ln -s $DOTFILES/lftprc $HOME/.lftprc
