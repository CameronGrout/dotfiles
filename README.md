Hi!

This repository uses submodules to manage the pathogen bundles for vim plugins. If you want to clone this repo and have everything running immediately, use:

```
git clone --recursive https://bitbucket.org/CameronGrout/dotfiles
```

Otherwise, after cloning the repository as per normal, you will need to execute:

```
git submodule update --init --recursive
```
